# SPIT Students' Blog, 2023-24 (Git Activity)

This is an activity to learn about **git** and **git** platforms.

The contents of this repository are contributed by SPIT students.

## [Index of Articles](/index.md)

![SPIT FE](images/fe.webp)
Students of Batch F1, F2, F3 & F4

## Published articles

[https://buzzworthy.co.in/blog](https://buzzworthy.co.in/blog) 

## Getting Started

1. Download and install Git from the following link:

[Git Official Website](https://git-scm.com/)

2. Download and install VSCode or an equivalent text editor.

[VSCode Official Website](https://code.visualstudio.com/)


### How to submit my blog article?

1. Fork the project
2. Clone locally or open using web IDE.
3. Make changes, i.e. add your article with your name and details.
4. Submit by creating a merge request.

Click to watch an instructional video about the same.

*Tutorial on making merge requests for article contribution.*

[![Git Tutorial | 2. Creating Pull Request/Merge Request in GitLab](https://img.youtube.com/vi/EtMm1BhVmtw/mqdefault.jpg)](https://youtu.be/EtMm1BhVmtw?si=uNeCi8vNM_PXRpi7)

*Bonus! Tutorial on accepting merge requests.*

[![Git Tutorial | 3. Accepting Merge/Pull Requests](https://img.youtube.com/vi/Kgh-9YzKAs8/mqdefault.jpg)](https://youtu.be/Kgh-9YzKAs8?si=QL5v8P6vrdCbdtlg)


**Please note:** Latest submissions to be made using merge requests only.
